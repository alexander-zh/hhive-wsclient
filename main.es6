#!/usr/bin/env node

const WebSocketClient = require('websocket').client;
const randomString = require('random-string');


const url = 'ws://hhive.local/ws/994/lbaojds4/websocket';
const server = 'ws://hhive.local';
const headers = {
    'Cookie': 'sessionid=hxjri23bgq9d2p7qe4a3pkj80mzopnit',
    'Host': 'hhive.local',
};
const origin = 'http://hhive.local';
const clientsNumber = 100;
const clientTimeout = 180 * 1000;
const debug = false;


function logInfo(msg) {
    if (debug === true) {
        console.log(msg);
    }
}


let failCounter = 0;
let successCounter = 0;
for (let i = 0; i < clientsNumber; i++){
    const client = new WebSocketClient();
    client.on('connectFailed',(error) => console.log('Connect Error: ' + error.toString()));
    client.on('connect', (connection) => {
        logInfo('WebSocket Client Connected');
        const clientTimeoutId = setTimeout(() => {
            connection.close();
            failCounter += 1;
            console.log('TIMEOUT ' + failCounter);
        }, clientTimeout);
        connection.on('error', (error) => {console.log("WebSocket Connection Error: " + error.toString());});
        connection.on('close', () => logInfo('WebSocket Connection Closed. ' + connection.closeDescription));
        connection.on('message', (message) => {
            let messageData;
            if (message.type === 'utf8') {
                messageData = message.utf8Data;
            } else {
                messageData = message;
            }
            logInfo("Received: '" + messageData + "'");

            // Подписываемся после приветствия от сервера
            if (messageData === 'o') {
                const msg_sub = '["{\\"exchange\\":\\"subscribe\\",\\"kwargs\\":{\\"channel\\":\\"update\\"}}"]';
                logInfo('Send: ' + msg_sub);
                connection.sendUTF(msg_sub);
            }

            // Ждем последнего сообщения
            if (messageData !== 'h' && messageData !== 'o') {
                const finished = JSON.parse(JSON.parse(messageData.slice(1))[0]).payload.finished;
                if (finished === true) {
                    clearTimeout(clientTimeoutId);
                    logInfo('WELL DONE');
                    successCounter += 1;
                    connection.close();
                    if (successCounter === clientsNumber) {
                        console.log('WELL DONE ' + successCounter);
                    }
                }
            }
        });
    });

    const randomNumber = Math.floor(Math.random() * 1000);
    const url = `${server}/ws/${randomNumber}/${randomString()}/websocket`;
    client.connect(url, null, origin, headers);
}
